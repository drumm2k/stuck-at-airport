﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Work : MonoBehaviour {

	public bool isWorking = false;
	public bool isWorkCheck = false;

	public float lossEnergy = 3f;
	public float lostHunger = 2f;
	public float lostThirst = 2f;
	public float lossHappiness = 2f;

	public float WorkRate = 1f;
	public float workDone = 100f;
	public static float workCompleted = 0f;
	public float workGain = 10f;

	public static float startWork = 0f;

	public static float tierLevelStart = 1f;
	public static float experienceStart = 0f;
	public static float tierLevelNow = 2f;
	public float tierLevelMax = 5f;
	public static float experienceNow = 0f;
	public float experienceMax = 100f;

	float tier1Exp = 10f;
	float tier2Exp = 5f;
	float tier3Exp = 3f;
	float tier4Exp = 2f;
	float tier5Exp = 1f;

	float moneyGain1 = 20f;
	float moneyGain2 = 30f;
	float moneyGain3 = 50f;
	float moneyGain4 = 70f;
	float moneyGain5 = 100f;

	private GameManager gm;
	private TextBox_Spawner textspawner;
	public Image workBarFill;

	public Text buttonWorkText;

	public Text tierText;
	public Text expText;

//	string buttonText = "Start";

	void Awake () {
		gm = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
		textspawner = GameObject.FindObjectOfType(typeof(TextBox_Spawner)) as TextBox_Spawner;
		workBar ();
		workSetText ();
	}

	public void Working()
	{
		if (!isWorking)
		{
			isWorking = true;
			InvokeRepeating ("addW", WorkRate, WorkRate);
			GetComponentInChildren<Text>().text = "Stop";
		}
		else {
			isWorking = false;
			CancelInvoke ("addW");
			GetComponentInChildren<Text>().text = "Start";
		}
	}

	public void addW()
	{
		if (GameManager.currentEnergy <= 10){
			stopWork();
			textspawner.MakeSpawn ("I need to sleep");
			return;
		}
		if (GameManager.currentHunger <= 10){
			stopWork();
			textspawner.MakeSpawn ("I'm hungry");
			return;
		}
		if (GameManager.currentThirst <= 10){
			stopWork();
			textspawner.MakeSpawn ("I'm thirsty");
			return;
		}
		if (GameManager.currentHappiness <= 10){
			stopWork();
			textspawner.MakeSpawn ("I'm not happy");
			return;
		}
			StartCoroutine ("addWork");
	}

	IEnumerator addWork()
	{
		if (isWorkCheck)
			yield break;

		isWorkCheck = true;

		GameManager.currentEnergy -= lossEnergy;
		GameManager.currentHappiness -= lossHappiness;
		GameManager.currentHunger -= lostHunger;
		GameManager.currentThirst -= lostThirst;

		if (workCompleted >= workDone) {
			workCompleted = 0f;
			if (tierLevelNow == 1f) {
				GameManager.currentMoney += moneyGain1;
				textspawner.MakeSpawn ("+" + moneyGain1 + "$");
				workSetExp (tier1Exp);
				workSetText ();
			}
			if (tierLevelNow == 2f) {
				GameManager.currentMoney += moneyGain2;
				textspawner.MakeSpawn ("+" + moneyGain2 + "$");
				workSetExp (tier2Exp);
				workSetText ();
			}
			if (tierLevelNow == 3f) {
				GameManager.currentMoney += moneyGain3;
				textspawner.MakeSpawn ("+" + moneyGain3 + "$");
				workSetExp (tier3Exp);
				workSetText ();
			}
			if (tierLevelNow == 4f) {
				GameManager.currentMoney += moneyGain4;
				textspawner.MakeSpawn ("+" + moneyGain4 + "$");
				workSetExp (tier4Exp);
				workSetText ();
			}
			if (tierLevelNow == 5f) {
				GameManager.currentMoney += moneyGain5;
				textspawner.MakeSpawn ("+" + moneyGain5 + "$");
				workSetExp (tier5Exp);
				workSetText ();
			}
		}
		if (workCompleted < workDone)
			workCompleted += workGain;		

		workBar ();
		gm.plusTime ();

		isWorkCheck = false;
	}

	public void stopWork(){
		isWorking = false;
		CancelInvoke ("addW");
		buttonWorkText.text = "Start";
	}

	void workBar() {
		float calc_work = workCompleted / workDone; //0.0 - 1.0
		setWorkBar (calc_work);
	}

	void setWorkBar(float worknow)
	{
		workBarFill.fillAmount = worknow;
	}

	void workSetExp(float addexp) {
		experienceNow = experienceNow + addexp;
		if (experienceNow >= experienceMax) {
			experienceNow = 0f;
			tierLevelNow = tierLevelNow + 1f;
		}
	}

	void workSetText() {
		tierText.text = ("Tier " + tierLevelNow);
		expText.text = ("Experience: " + experienceNow + "/" + experienceMax);
	}

}