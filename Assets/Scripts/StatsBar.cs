﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsBar : MonoBehaviour {

	public Image hungerBar;
	public Image thirstBar;
	public Image happBar;
	public Image energyBar;

	// Use this for initialization
	void Update () {
		HungerBar();
	}

	void HungerBar() {
		float calc_hunger = GameManager.currentHunger / GameManager.maxHunger; //0.0 - 1.0
		float calc_thirst = GameManager.currentThirst / GameManager.maxThirst; //0.0 - 1.0
		float calc_happ = GameManager.currentHappiness / GameManager.maxHappiness; //0.0 - 1.0
		float calc_energy = GameManager.currentEnergy / GameManager.maxEnergy; //0.0 - 1.0
		setHungerBar (calc_hunger);
		setThirstBar (calc_thirst);
		setHappBar (calc_happ);
		setEnergyBar (calc_energy);
	}

	void setHungerBar(float hungernow)
	{
		hungerBar.fillAmount = hungernow;
	}

	void setThirstBar(float thirstnow)
	{
		thirstBar.fillAmount = thirstnow;
	}

	void setHappBar(float happnow)
	{
		happBar.fillAmount = happnow;
	}

	void setEnergyBar(float energynow)
	{
		energyBar.fillAmount = energynow;
	}
}
