﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Sos : MonoBehaviour {

	public Text available;

	public void watchAD (){
		if (GameManager.watchedAd == 0f) {

			if (Advertisement.IsReady ()) {
				Advertisement.Show ();
				GameManager.watchedAd = 1f;

				if (GameManager.currentHunger <= 50f)
					GameManager.currentHunger = 50f;

				if (GameManager.currentThirst <= 50f)
					GameManager.currentThirst = 50f;

				if (GameManager.currentHappiness <= 50f)
					GameManager.currentHappiness = 50f;

				if (GameManager.currentEnergy <= 50f)
					GameManager.currentEnergy = 50f;

				available.text = ("Not Available");
			}
		}
	}
}