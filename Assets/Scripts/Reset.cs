﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {

	public GameObject continueButton;

	void Awake () {
		if (continueButton == null)
			Debug.LogError ("Need to set continueButton.");

		if (PlayerPrefs.HasKey ("Save_Money")) {
			if (PlayerPrefs.GetFloat ("Save_Money") > 0)
				continueButton.SetActive (true);
		}
	}

	public void resetProgress () {
		PlayerPrefs.DeleteKey ("Save_Hunger");
		PlayerPrefs.DeleteKey ("Save_Thirst");
		PlayerPrefs.DeleteKey ("Save_Happiness");
		PlayerPrefs.DeleteKey ("Save_Energy");
		PlayerPrefs.DeleteKey ("Save_Day");
		PlayerPrefs.DeleteKey ("Save_Time");
		PlayerPrefs.DeleteKey ("Save_Money");
		PlayerPrefs.DeleteKey ("Save_Work");
		PlayerPrefs.DeleteKey ("Save_Work_Tier");
		PlayerPrefs.DeleteKey ("Save_Work_Exp");
		PlayerPrefs.DeleteKey ("Save_SOS_Ad");

//		PlayerPrefs.DeleteAll();
	}

}
