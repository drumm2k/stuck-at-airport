﻿using UnityEngine;
using System.Collections;

public class LoadOnClick : MonoBehaviour {

	public string levelToLoad;

	public void LoadClick() {
		Application.LoadLevel(levelToLoad);
	}
}
