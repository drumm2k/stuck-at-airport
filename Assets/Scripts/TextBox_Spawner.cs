﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBox_Spawner : MonoBehaviour {

	public GameObject spawnObjects; 
	private Text txtRef;

	void Start () {
//		MakeSpawn ("test");
	}

	public void MakeSpawn (string message)
	{
		GameObject spawnedObject = Instantiate (spawnObjects, Vector3.zero, Quaternion.identity) as GameObject;
		spawnedObject.transform.SetParent(GameObject.Find("TextSpawner").gameObject.transform, false);
		txtRef = spawnedObject.GetComponent<Text> ();
		txtRef.text = message;
	}
}
