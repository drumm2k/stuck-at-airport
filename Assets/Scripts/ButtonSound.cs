﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour {

	public AudioClip sound;

	private Button button { get { return GetComponent<Button> (); } }
	private AudioSource source { get { return GetComponent<AudioSource> (); } }

	// Use this for initialization
	void Start () {
		gameObject.AddComponent<AudioSource> ();
		source.clip = sound;
		source.playOnAwake = false;
		button.onClick.AddListener (() => PlaySound ());
	}
	
	void PlaySound (){
		source.volume = 0.7f;
		source.PlayOneShot (sound);
	}

}
