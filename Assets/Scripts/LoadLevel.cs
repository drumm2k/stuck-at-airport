﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public string levelToLoad;
	public float delay = 2f;

	// use invoke to wait for a delay then call LoadLevel
	void Start () {
		Invoke("Load",delay);
	}

	// load the specified level
	void Load() {
		Application.LoadLevel(levelToLoad);
	}
		
}
