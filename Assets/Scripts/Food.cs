﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Food : MonoBehaviour {

	public float foodGainHunger = 1f;
	public float foodGainThirst = 1f;
	public float foodGainHappiness = 1f;
	public float foodGainEnergy = 1f;
	public float foodPrice = 5f;

	private TextBox_Spawner textspawner;

	void Awake () {
		textspawner = GameObject.FindObjectOfType(typeof(TextBox_Spawner)) as TextBox_Spawner;
	}

	public void eat() {
		if (GameManager.currentMoney < foodPrice)
			textspawner.MakeSpawn ("Not enough money");
		else {
			GameManager.currentHunger += foodGainHunger;
			GameManager.currentThirst += foodGainThirst;
			GameManager.currentHappiness += foodGainHappiness;
			GameManager.currentEnergy += foodGainEnergy;
			GameManager.currentMoney -= foodPrice;
		}
	}
}
