﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sleep : MonoBehaviour {

	public bool isSleeping = false;
	public bool isSleepCheck = false;
	public float gainEnergy = 5f;
	public float lostHunger = 2f;
	public float lostThirst = 1f;
	public float gainEnergyRate = 1f;
	public float gainHappiness = 2f;

	private GameManager gm;
	private Text text;
	private TextBox_Spawner textspawner;

//	string buttonText = "Start";

	void Awake () {
		gm = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
		text = transform.FindChild("Text").GetComponent<Text>();
		textspawner = GameObject.FindObjectOfType(typeof(TextBox_Spawner)) as TextBox_Spawner;
	}

	public void Sleeping()
	{
		if (!isSleeping)
		{
			isSleeping = true;
			InvokeRepeating ("addE", gainEnergyRate, gainEnergyRate);
			text.text = "Stop";
		}
		else {
			isSleeping = false;
			CancelInvoke ("addE");
			text.text = "Sleep";
		}
	}

	public void addE()
	{
		StartCoroutine ("addEnergy");
	}

	IEnumerator addEnergy()
	{
		if (isSleepCheck)
			yield break;

		isSleepCheck = true;

//		yield return new WaitForSeconds(gainEnergyRate);
		if (GameManager.currentHunger <= 10){
			isSleepCheck = false;
			text.text = "Sleep";
			textspawner.MakeSpawn ("I'm hungry");
			CancelInvoke ("addE");
			yield break;
		}
		if (GameManager.currentThirst <= 10){
			isSleepCheck = false;
			text.text = "Sleep";
			textspawner.MakeSpawn ("I'm thirsty");
			CancelInvoke ("addE");
			yield break;
		}
		GameManager.currentEnergy += gainEnergy;
		GameManager.currentHappiness += gainHappiness;
		GameManager.currentHunger -= lostHunger;
		GameManager.currentThirst -= lostThirst;

		gm.plusTime ();


		isSleepCheck = false;
	}

	public void stopSleeping(){
		isSleeping = false;
		CancelInvoke ("addE");
		text.text = "Sleep";
	}
		
}
