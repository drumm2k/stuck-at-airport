﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class GameManager : MonoBehaviour {

	public static GameManager gm;

	public static float currentHealth = 100f;
	public static float maxHealth = 100f;

	public static float currentHunger = 50f;
	public static float maxHunger = 100f;

	public static float currentThirst = 50f;
	public static float maxThirst = 100f;

	public static float currentHappiness = 50f;
	public static float maxHappiness = 100f;

	public static float currentEnergy = 50f;
	public static float maxEnergy = 100f;

	public static float currentMoney = 50f;

	public static float currentDay = 6f;

	float startDay = 1f;
	float startMoney = 80f;
	float startHunger = 50f;
	float startThirst = 50f;
	float startHappiness = 50f;
	float startEnergy = 50f;


//	public float hungerFallRate = 4f;
//	public float thirstFallRate = 4f;

	public Text UIHealth;
	public Text UIHunger;
	public Text UIThirst;
	public Text UIHappiness;
	public Text UIEnergy;
	public Text UIMoney;
	public Text UIDay;

	public Image clockFill;
	public float max_time = 100f;
	public float cur_time = 100f;
	public float add_time = 5f;
	public bool isTicking = false;

	public float sendAmount = 100f;
	public float beg = 3f;
	public float begIncome = 30f;

	public GameObject weekSummaryUI;
	public bool weekSummaryOpened;
	public Text weekNumber;
	public Text weekEarned;
	public Text weekSpent;
	public Text weekFamily;
	public Text weekMoneyNow;

	private TextBox_Spawner textspawner;
	private Sleep sleep;
	private Work work;

	public GameObject workUI;
	public GameObject shopUI;

	public static float watchedAd = 0f;
	public Text advText;

	void Awake () {
		if (gm == null)
			gm = this.GetComponent<GameManager>();

		setupDefaults ();
		autoLoad ();
		setTime ();

		sleep = GameObject.FindObjectOfType(typeof(Sleep)) as Sleep;
		textspawner = GameObject.FindObjectOfType(typeof(TextBox_Spawner)) as TextBox_Spawner;
		workUI.SetActive (true);
		work = GameObject.FindObjectOfType(typeof(Work)) as Work;
		workUI.SetActive (false);
	}

	void Start () {
		if (watchedAd == 1f)
			advText.text = ("Not Available");
	}

	// Update is called once per frame
	void Update () {
//		if (currentHealth <= 0) {
//		}

//		if(currentHunger >= 0)
//			currentHunger = currentHunger - 1f;

//		if (currentHunger <= 0) 
//			currentHunger = 0;
		if (currentHealth >= maxHealth)
			currentHealth = maxHealth;
		
		if (currentHunger >= maxHunger)
			currentHunger = maxHunger;

		if (currentThirst >= maxThirst)
			currentThirst = maxThirst;

		if (currentHappiness >= maxHappiness)
			currentHappiness = maxHappiness;

		if (currentEnergy >= maxEnergy)
			currentEnergy = maxEnergy;

		if (currentHunger < 0)
			currentHunger = 0;

		if (currentThirst < 0)
			currentThirst = 0;

		if (currentHappiness < 0)
			currentHappiness = 0;

		if (currentEnergy < 0)
			currentEnergy = 0;

		refreshGUI ();
	}

	void setupDefaults() {
		if (UIHealth == null)
			Debug.LogError ("Need to set UIHealth on Game Manager.");
		
		if (UIHunger == null)
			Debug.LogError ("Need to set UIHunger on Game Manager.");

		if (UIThirst == null)
			Debug.LogError ("Need to set UIThirst on Game Manager.");

		if (UIHappiness == null)
			Debug.LogError ("Need to set UIHappiness on Game Manager.");
		
		if (UIEnergy == null)
			Debug.LogError ("Need to set UIEnergy on Game Manager.");

		if (UIMoney == null)
			Debug.LogError ("Need to set UIMoney on Game Manager.");

		if (UIDay == null)
			Debug.LogError ("Need to set UIDay on Game Manager.");

		if (clockFill == null)
			Debug.LogError ("Need to set clockFill on Game Manager.");

		if (weekSummaryUI == null)
			Debug.LogError ("Need to set weekSummaryUI on Game Manager.");

		// get the UI ready for the game
//		refreshGUI ();
	}

	void refreshGUI() {
		// set the text elements of the UI
/*		UIHealth.text = "Health: "+ currentHealth.ToString();
		UIHunger.text = "Hunger: "+ currentHunger.ToString();
		UIThirst.text = "Thirst: "+ currentThirst.ToString();
		UIHappiness.text = "Happiness: "+ currentHappiness.ToString();
		UIEnergy.text = "Energy: "+ currentEnergy.ToString();
*/
		UIMoney.text = currentMoney.ToString() +"$";
		UIDay.text = "Day "+ currentDay.ToString();

		//		UILevel.text = Application.loadedLevelName;

	}

	public void plusTime()
	{
		//	StartCoroutine ("addTime");
		if (cur_time == 0f) {
			cur_time = max_time;
			currentDay += 1;
			adv ();

			if ((currentDay + 1) % 7 == 0) {
				textspawner.MakeSpawn ("Tomorrow you should send " + sendAmount + "$ to your family.");
			}
			if (currentDay % 7 == 0) {
				weekSummaryOpened = true;
				weekSummary ();
			}
		}
		cur_time -= add_time;
		setTime ();
		autoSave ();
	}

/*	IEnumerator addTime()
	{
		if (isTicking)
			yield break;

		isTicking = true;

		yield return new WaitForSeconds(1f);

		if (cur_time == 0f)
			cur_time = max_time;
		cur_time -= add_time;
		float calc_time = cur_time / max_time; //0.0 - 1.0
		setTime (calc_time);

		isTicking = false;
	}
*/

	void setTime()
	{
		float calc_time = cur_time / max_time; //0.0 - 1.0
		clockFill.fillAmount = calc_time;
	}

	void weekSummary()
	{
		sleep.stopSleeping ();

		work.stopWork ();
		workUI.SetActive (false);

		shopUI.SetActive (false);

		currentMoney = currentMoney - sendAmount;

		if (currentMoney < 0) {
			
		}
			

/*		if (currentMoney < 0) {
			textspawner.MakeSpawn ("You can't help your family this time... Try again?");
			Invoke ("gameOver", 3f);
			return;
		}*/
/*			if (currentMoney < 0) {
				begMoney ();
				return;
			}
*/			
		weekSummaryUI.SetActive (true);
//			textspawner.MakeSpawn("You send " + sendAmount + "$ to your family. They are happy!");
		weekNumber.text = ("Week " + (currentDay / 7) + " Summary");
		weekEarned.text = ("Earned: " + "$");
		weekSpent.text = ("Spent: " + "$");
		weekFamily.text = ("Send to Family: " + sendAmount + "$");
		weekMoneyNow.text = ("You have now: " + currentMoney + "$");

//			sendMoneyOpened = false;
//		}
	}

	void begMoney()
	{
		float begCalc = currentMoney + begIncome;
		if (beg > 0 && begCalc > 0){
			currentMoney = currentMoney + begIncome;
			beg = beg - 1f;
			textspawner.MakeSpawn ("You beg some strangers and get " + begIncome + "$. This helps your family.");
			Debug.Log ("Sent + Beg");
		}
		if (beg > 0 && begCalc < 0) {
			textspawner.MakeSpawn ("You beg some strangers and get " + begIncome + "$. This doesn't enought to support your family.");
			Invoke ("gameOver", 2f);
		}
		if (beg <= 0) {
			textspawner.MakeSpawn ("You tried to beg, nobody helped.");
			Invoke ("gameOver", 2f);
		}
	}

	public void gameOver(){
		Application.LoadLevel ("Scene_Menu");
	}

	void autoSave(){
		PlayerPrefs.SetFloat ("Save_Game", 1f);

		PlayerPrefs.SetFloat ("Save_Hunger", currentHunger);
		PlayerPrefs.SetFloat ("Save_Thirst", currentThirst);
		PlayerPrefs.SetFloat ("Save_Happiness", currentHappiness);
		PlayerPrefs.SetFloat ("Save_Energy", currentEnergy);
		PlayerPrefs.SetFloat ("Save_Day", currentDay);
		PlayerPrefs.SetFloat ("Save_Time", cur_time);
		PlayerPrefs.SetFloat ("Save_Money", currentMoney);
		PlayerPrefs.SetFloat ("Save_Work", Work.workCompleted);

		PlayerPrefs.SetFloat ("Save_Work_Tier", Work.tierLevelNow);
		PlayerPrefs.SetFloat ("Save_Work_Exp", Work.experienceNow);

		PlayerPrefs.SetFloat ("Save_SOS_Ad", watchedAd);

		PlayerPrefs.Save ();
//		Debug.Log ("Saved");
	}

	void autoLoad(){
		if (PlayerPrefs.HasKey ("Save_Hunger")) {
			currentHunger = PlayerPrefs.GetFloat ("Save_Hunger");
		} else
			currentHunger = startHunger;

		if (PlayerPrefs.HasKey ("Save_Thirst")) {
			currentThirst = PlayerPrefs.GetFloat ("Save_Thirst");
		} else
			currentThirst = startThirst;

		if (PlayerPrefs.HasKey ("Save_Happiness")) {
			currentHappiness = PlayerPrefs.GetFloat ("Save_Happiness");
		} else
			currentHappiness = startHappiness;

		if (PlayerPrefs.HasKey ("Save_Energy")) {
			currentEnergy = PlayerPrefs.GetFloat ("Save_Energy");
		} else
			currentEnergy = startEnergy;

		if (PlayerPrefs.HasKey ("Save_Day")) {
			currentDay = PlayerPrefs.GetFloat ("Save_Day");
		} else
			currentDay = startDay;

		if (PlayerPrefs.HasKey ("Save_Time")) {
			cur_time = PlayerPrefs.GetFloat ("Save_Time");
		} else
			cur_time = max_time;

		if (PlayerPrefs.HasKey ("Save_Money")) {
			currentMoney = PlayerPrefs.GetFloat ("Save_Money");
		} else
			currentMoney = startMoney;

		if (PlayerPrefs.HasKey ("Save_Work")) {
			Work.workCompleted = PlayerPrefs.GetFloat ("Save_Work");
		} else
			Work.workCompleted = Work.startWork;

		if (PlayerPrefs.HasKey ("Save_Work_Tier")) {
			Work.tierLevelNow = PlayerPrefs.GetFloat ("Save_Work_Tier");
		} else
			Work.tierLevelNow = Work.tierLevelStart;
	
		if (PlayerPrefs.HasKey ("Save_Work_Exp")) {
			Work.experienceNow = PlayerPrefs.GetFloat ("Save_Work_Exp");
		} else
			Work.experienceNow = Work.experienceStart;

		if (PlayerPrefs.HasKey ("Save_SOS_Ad")) {
			watchedAd = PlayerPrefs.GetFloat ("Save_SOS_Ad");
		} else
			watchedAd = 0f;
	}

	void adv (){
	watchedAd = 0f;
	advText.text = ("Available");
	}
}
